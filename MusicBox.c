#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#define MAX 256
struct songs
{
    char name[MAX];
    int length;
    struct songs *next;
};
typedef struct songs songs;

struct albums
{
    char title[MAX];
    int year;
    char singerName[MAX];
    songs *bas;
    songs *current;
    struct albums *next;
};

struct albums *head = NULL;
struct albums *curr = NULL;

songs *bas=NULL;
songs *current=NULL;

int check(char albumTitle[])
{
    struct albums *temp;
    temp=(struct albums *)malloc(sizeof(struct albums));
    temp=head;
    while(temp!=NULL)
    {
        if(!(strcmp(temp->title, albumTitle)))
            return 0;
        else
            temp=temp->next;
    }
    return 1;
}
int checkSong(char albumTitle[],char SongName[])
{
	struct albums *temp;
    temp=(struct albums *)malloc(sizeof(struct albums));
    temp=head;
    songs *tempsong;
    tempsong=(struct songs *)malloc(sizeof(struct songs));
    
    while(temp!=NULL)
    {
        if(!(strcmp(temp->title, albumTitle)))
        {
                tempsong=temp->bas ;         
                while(tempsong!=NULL)
				{
				 if(!(strcmp(tempsong->name, SongName)))
				   {
					return 1;
                   }	
				  else{
					tempsong=tempsong->next;
				  }
                }
				return 0;
        }
        else
        {
            temp= temp->next;
        }
    }
	return 0;
    
}
int checkYear(int year)
{
	struct albums *temp;
    temp=(struct albums *)malloc(sizeof(struct albums));
    temp=head;
	while(temp!=NULL)
    {
        if(year<(temp->year))
            return 0;
        else
            temp=temp->next;
    }
    return 1;


}
void add(char albumTitle[],char singerName[], int releaseYear )
{
    struct albums *temp;
    temp=(struct albums *)malloc(sizeof(struct albums));
    strcpy( temp->title, albumTitle);
    strcpy( temp->singerName, singerName );
    temp->year=releaseYear;
    if (head== NULL)
    {
    curr=head=temp;
    head->next=NULL;
    curr->next=NULL;
	printf("Done\n");
    }
     else
    {
        if(check(albumTitle)&&checkYear(releaseYear)){
        curr->next=temp;
        curr=temp;
		printf("Done\n");
        }
        else
         printf("It is  forbidden\n");   
    }
    
    
}
int removeAlbum(char albumTitle[])
{
    struct albums *temp, *prev;
    temp=head;
    
    while(temp!=NULL)
    {
    if(!(strcmp(temp->title, albumTitle)))
    {
        if(temp==head)
        {
        head=temp->next;
        free(temp);
        return 1;
        }
        else
        {
        prev->next=temp->next;
        free(temp);
        return 1;
        }
    }
    else
    {
        prev=temp;
        temp= temp->next;
    }
    }
    return 0;
}
void ShowAllAlbums()
{
    struct albums *r;
    r=head;
    int i =1;
	if(r==NULL){
	printf("--none--");
	}
    while(r!=NULL)
    {
    printf("%s,%d,%s,(for %d album)\n",r->title,r->year,r->singerName,i);
    r=r->next;
    i++;
    }
    printf("\n");
}
void showAlbum( char albumTitle[] )
{
    struct albums *temp;

    temp=head;
    while(temp!=NULL)
    {
        if(!(strcmp(temp->title, albumTitle)))
        {
            printf("--------\n");
            printf("%s\n",temp->title );
            printf("%d\n",temp->year );
            printf("%s\n",temp->singerName );
            /**This part is the same ShowAllAlbums method.*/
            struct songs *r;
            r=temp->bas;
            

            while(r!=NULL)
            {
			int d=1;
            printf("%s,%d(for %d song)\n",r->name,r->length,d);
            r=r->next;
            d++;
            }
            /***/ 
            printf("-------\n");
            break;
        }
        else
        {
            temp=temp->next;
        }
    }

}
void addSong(char albumTitle[],char songName[], double songLength )
{
    struct albums *temp;
    temp=(struct albums *)malloc(sizeof(struct albums));
    temp=head;
    songs *tempsong;
    tempsong=(struct songs *)malloc(sizeof(struct songs));
    
    while(temp!=NULL)
    {
        if(!(strcmp(temp->title, albumTitle)))
        {
                          
            strcpy(tempsong->name,songName);
            tempsong->length=songLength;
                if (temp->bas== NULL)
                {
                temp->current=temp->bas=tempsong;
                temp->bas->next=NULL;
                temp->current->next=NULL;
                }
                 else
                {
                 temp->current->next=tempsong;
                 temp->current=tempsong;
                }
            break;    
        }
        else
        {
            temp= temp->next;
        }
    }
	printf("Done\n");
    
}
int removeSong(char albumTitle[],char songName[])
{
    struct albums *temp1,*temp2;
    temp1=head;
    songs *temp,*prev;
    
    temp=temp1->bas;
    
    while(temp!=NULL)
    {
        if(!(strcmp(temp1->title, albumTitle)))
        {
                          
            if(temp==temp1->bas)
            {
                head->bas=temp->next;
                free(temp);
                return 1;
            }
            else
            {
                prev->next=temp->next;
                free(temp);
            return 1;
            }
        }
        else
        {
            temp= temp->next;
        }
    }
    return 0;
}
void showSingerAlbums(char singerName[])
{
    struct albums *temp;
    int i=1;
    temp=head;
    printf("--------\n");
    printf("%s\n",singerName );
	
    while(temp!=NULL)
    {
        if(!(strcmp(temp->singerName, singerName)))
        {
            
            printf("%s,%d (for the %d album)\n",temp->title,temp->year ,i);
            
            
            i++;
            temp=temp->next;
        }
        else
        {
            temp=temp->next;
        }
    }
    printf("--------\n");

}
void showSongsWithParticularLength(int LowerBoundary, int UpperBoundary )
{
    int sayi =0;
	int count=1;
    struct albums *temp;
    temp=(struct albums *)malloc(sizeof(struct albums));
    temp=head;
    songs *tempsong;
    tempsong=(struct songs *)malloc(sizeof(struct songs));
    tempsong=head->bas;
    
    while(temp!=NULL)
    {
        while(tempsong!=NULL)
        {
			
			sayi=tempsong->length;
            if(sayi > LowerBoundary)
            {
				
				if(sayi < UpperBoundary)
				{
                printf("%s,%d (for the %d album)\n",tempsong->name,tempsong->length,count );
				count++;
                
				}
            }
			
            tempsong=tempsong->next;
			
        }
        
            temp=temp->next;
    }
	if(count==1)
	{
	printf("---none---\n");
	}
	printf("Done\n");
}

int main(int argc, char const *argv[])
{
    a:
    printf("1. Add an album\n");
    printf("2. Remove an album\n");
    printf("3. Show the master-linked-list\n");
    printf("4. Show detailed information about a particular album\n");
    printf("5. Add a song to the song list of an album\n");
    printf("6. Remove a song from the song list of a album\n");
    printf("7. Query the albums which were released by a particular singer\n");
    printf("8. Query the songs whose lengths are in a particular scope\n");
    printf("9. Exit\n");
    char albumTitle[MAX]="";
    char singerName[MAX]="";
    char songName[MAX]="";
    int songLength;
	int LowerBoundary,UpperBoundary;
    int releaseYear=0;

    while(1)
    {
        int choose;

        scanf("%d",&choose);
        if(choose==1)
                {  
                  printf("Please enter the album title\n");
                  scanf(" %[^\n]%*c",&albumTitle);
                  printf("Please enter the singer name\n");
                  scanf (" %[^\n]%*c",&singerName);
                  printf("Please enter the release year\n");
                  scanf("%d",&releaseYear);
                    add(albumTitle,singerName,releaseYear );
                goto a;
                }
        else if(choose==2)
        {
             
                  printf("Please enter the album title\n");
                  b:
                  scanf(" %[^\n]%*c",&albumTitle);
                  if(removeAlbum(albumTitle))
                  {
                   printf("Remove album\n");
                   goto a;                          
                  }
                  else
                  {
                   printf("Doesn't have album title, please enter new album title\n");
                   goto b;
                  }
        }
        else if(choose==3)
        {
                 ShowAllAlbums();
                 goto a;      
        }
        else if(choose==4)
        {
                 printf("Please enter the album title\n");
                 scanf(" %[^\n]%*c",&albumTitle);
				 if(!check(albumTitle))
					{
					showAlbum(albumTitle);
					}
				 else
					{
					printf("--none--\n");
					}
                 goto a;
        }
        else if(choose==5)
        {
                printf("Please enter the album title\n");
                scanf(" %[^\n]%*c",&albumTitle);
                if(!check(albumTitle)){
					printf("Please enter the song name\n");
					scanf(" %[^\n]%*c",&songName);
					if(!checkSong(albumTitle,songName))
					{
						printf("Please enter the song length\n");
						scanf("%d",&songLength);
					addSong(albumTitle,songName,songLength);
					}
					else
					{
					printf("This song name exist.\n");
					goto a;
					}
				}
				else
				{
				printf("This album title does not exist.Not allow the operation\n");
				}
                goto a;
        }
        else if(choose==6)
        {
                printf("Please enter the album title\n");
				scanf(" %[^\n]%*c",&albumTitle);
                  if(!check(albumTitle)){
					printf("Please enter the song name\n");
					scanf(" %[^\n]%*c",&songName);
					if(checkSong(albumTitle,songName))
					{
						removeSong(albumTitle,songName);
						printf("Deleted\n");
					}
					else
					{
					printf("This song name does not exist.\n");
					goto a;
					}
				}
				else
				{
				printf("This album title does not exist.Not allow the operation\n");
				}
                goto a;
        }
        else if(choose==7)
        {
            printf("Please enter the Singer name\n");
            scanf(" %[^\n]%*c",&singerName);
            showSingerAlbums(singerName);
            goto a;
        }
        else if(choose==8)
        {
            printf("Please enter the LowerBoundary\n");
            scanf("%d",&LowerBoundary);
            printf("Please enter the UpperBoundary\n");
            scanf("%d",&UpperBoundary);
            showSongsWithParticularLength(LowerBoundary,UpperBoundary );
            goto a;
        }
        else if(choose==9)
        {
        break;     
        }
        else if(choose>9 || choose<1)
        {
            printf("Please enter the number between 1 and 8\n");
        }

    }
    return 0;
}
